const dropList=document.querySelectorAll(".drop-list select");

for(let i=0; i<dropList.length; i++){
    for(local_currency_code in country_code){
        // make an <option > tag based on the county list 
        let optionTag=`<option value='${local_currency_code}'>${local_currency_code}</option>`
        // insert this created <option> into the droplist 
        dropList[i].insertAdjacentHTML("Beforeend",optionTag);
    }
}