const deg=6;
const hr=document.getElementById('hr');
const mn=document.querySelector('#mn');
const sc=document.querySelector('#sc');
setInterval(()=>{
    
// measur the time
    let day= new Date();
    let hh=day.getHours()*30;
    let mm=day.getMinutes()*deg;
    let ss=day.getSeconds()*deg;

// add measured time to styls
    hr.style.transform=`rotateZ(${hh+(mm/12)}deg)`;
    mn.style.transform=`rotateZ(${(mm)}deg)`;
    sc.style.transform=`rotateZ(${(ss)}deg)`;

    
})